<?php

namespace App\Repositories;

interface PresenceRepositoryInterface
{
    public function createPresence(array $presenceData);
    public function getPayslips($month);
}
