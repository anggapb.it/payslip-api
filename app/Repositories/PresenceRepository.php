<?php

namespace App\Repositories;

use App\Constants\ComponentPayslipConstant;
use App\Models\Presence;
use Carbon\Carbon;

class PresenceRepository implements PresenceRepositoryInterface
{
    public function createPresence(array $presenceData)
    {
        return Presence::create($presenceData);
    }

    public function getPayslips($yearMonth)
    {
        $year = (int) substr($yearMonth, 0, 4);
        $month = (int) substr($yearMonth, 5);

        $presences = new Presence();  
        
        $gapok = 2000000;
        $tunjangan = $this->calculateComponentPayslip($presences, $year, $month, ComponentPayslipConstant::COMPONENT_SLIP['tunjangan']);  
        $potongan = $this->calculateComponentPayslip($presences, $year, $month, ComponentPayslipConstant::COMPONENT_SLIP['potongan']);  
        $takeHomePay = ($gapok + $tunjangan) - $potongan;

        return [
            "gapok"         => $gapok,
            "tunjangan"     => $tunjangan,
            "potongan"      => -1 * abs($potongan),
            "take_home_pay" => $takeHomePay
        ];
    } 
    
    private function calculateComponentPayslip($presences, $year, $month, $componentPayslip)
    {       
        $total = 0;

        if ($componentPayslip === ComponentPayslipConstant::COMPONENT_SLIP['tunjangan']) {
            $workingDays = $presences->whereRaw("YEAR(date) = ". $year)
                ->whereRaw("MONTH(date) = ". $month)
                ->whereRaw("TIME(date) <= '08:00:00'")
                ->count();    

            $totalPresenceOut = $presences->whereRaw("YEAR(date) = ". $year)
                ->whereRaw("MONTH(date) = ". $month)
                ->where('type', 'out')
                ->count();  
                
            if ($workingDays != $totalPresenceOut) {
                $workingDays = $workingDays - $totalPresenceOut;
            }    
                
            $amount = 15000;
            $total = $amount * $workingDays;   
        } 
        
        if ($componentPayslip === ComponentPayslipConstant::COMPONENT_SLIP['potongan']) {
            $totalLateFiveTeenMinutes = $presences->whereRaw("YEAR(date) = ". $year)
                ->whereRaw("MONTH(date) = ". $month)
                ->whereRaw("TIME(date) >= '08:15:00'")
                ->whereRaw("TIME(date) < '08:30:00'")
                ->where('type', 'in')
                ->count();
                
            if ($totalLateFiveTeenMinutes) {
                $amount = 5000;
                $total = $amount * $totalLateFiveTeenMinutes;
            }    
            
            $totalLateThirtyMinutes = $presences->whereRaw("YEAR(date) = ". $year)
                ->whereRaw("MONTH(date) = ". $month)
                ->whereRaw("TIME(date) >= '08:30:00'")
                ->whereRaw("TIME(date) <= '09:00:00'")
                ->where('type', 'in')
                ->count();

            if ($totalLateThirtyMinutes) {
                $amount = 10000;
                $total = $amount * $totalLateThirtyMinutes;
            }        
        }

        return $total;
    }


}
