<?php

namespace App\Constants;

/**
 * Class ComponentPayslipConstant.
 */
final class ComponentPayslipConstant
{
    public const COMPONENT_SLIP = [
        'gapok'             => 'Gaji Pokok',
        'tunjangan'         => 'Tunjangan Kinerja',
        'potongan'          => 'Potongan Keterlambatan'
    ];
}