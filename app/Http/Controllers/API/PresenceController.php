<?php

namespace App\Http\Controllers\API;

use App\Constants\ComponentPayslipConstant;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePresenceRequest;
use App\Http\Requests\GetPayslipRequest;
use App\Repositories\PresenceRepository;
use Illuminate\Http\Response;

class PresenceController extends Controller
{
    protected $presenceRepository;
    public function __construct(PresenceRepository $presenceRepository)
    {
        $this->presenceRepository = $presenceRepository;
    }
    public function store(CreatePresenceRequest $request)
    {
        return response()->json(
            [
                'data' => $this->presenceRepository->createPresence($request->validated())
            ],
            Response::HTTP_CREATED
        );
    }

    public function getPayslips(GetPayslipRequest $request)
    {
        $data = $this->presenceRepository->getPayslips($request->month);

        return response()->json(
            [
                'month' => $request->month,
                "components" => [
                    [
                        "name" => ComponentPayslipConstant::COMPONENT_SLIP['gapok'],
                        "amount" => $data['gapok']
                    ],
                    [
                        "name" => ComponentPayslipConstant::COMPONENT_SLIP['tunjangan'],
                        "amount" => $data['tunjangan']
                        
                    ],
                    [
                        "name" => ComponentPayslipConstant::COMPONENT_SLIP['potongan'],
                        "amount" => $data['potongan']

                    ]
                ],
                "take_home_pay" => $data['take_home_pay']
            ],
            Response::HTTP_OK
        );
    }
}
