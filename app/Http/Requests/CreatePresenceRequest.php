<?php

namespace App\Http\Requests;

use App\Rules\AttendanceDateRule;
use App\Rules\AttendanceTypeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreatePresenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => [
                'required',
                'string',
                'max:255',
            ],
            'type' => [
                'required',
                'string',
                Rule::in(['in', 'out']),
            ],
            'date' => [
                'required',
                'date_format:Y-m-d H:i:s',
                new AttendanceDateRule()
            ],
        ];
    }
}
