<?php

namespace Tests\Feature;

use App\Models\Presence;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetPayslipsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetPayslipSuccess()
    {
        $data = [
            'employee_id' => 'E001',
            'type' => 'in',
        ];

        $startDate = Carbon::parse('2023-01-01 07:45:00');
        $endDate = Carbon::parse($startDate)->endOfMonth();

        while ($startDate->lte($endDate)) {
            
            if ($startDate->isWeekday()) {
                $startDate->hour = 7;
                $startDate->minute = 45;

                $data['date'] = $startDate->toDateTimeString();
    
                $response = $this->post('/api/presences', $data);   
            }

            $startDate->startOfDay()->addDay(1);
        }

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->json('GET', '/api/payslips', ['month' => '2023-01']);

        $response->assertStatus(200);

        $response->assertJson(
            function (AssertableJson $json) {
                $json->where('month', '2023-01')
                    ->has('components')
                    ->has('take_home_pay')
                    ->etc();
            }
        );
    }
}
